defmodule Stresstest.MixProject do
  use Mix.Project

  def project do
    [
      app: :stresstest,
      version: "0.1.0",
      elixir: "~> 1.13",
      escript: [main_module: Stresstest],
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  def application do
    [applications: [:logger, :httpotion]]
  end

  defp deps do
    [
      {:ibrowse, "~> 4.2.2"},
      {:httpotion, "~> 2.1.0"},
      {:progress_bar, "> 0.0.0"},
    ]
  end
end
