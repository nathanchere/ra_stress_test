defmodule Worker do
  def call(agent, url, num_of_requests, opts) do
    work(url, agent, num_of_requests, opts)
  end

  defp work(_, _, 0, _) do
  end

  defp work(url, agent, requests_to_do, opts) do
    size = opts[:total_requests]
    try do
      start = :erlang.system_time() / 1000
      headers = Keyword.put(opts[:headers], :Connection, 'close')

      options = [headers: headers, ibrowse: [max_pipeline_size: size, max_sessions: size, ssl_options: [verify: :verify_none]]]
      response = HTTPotion.request(:get, url, options)

      finish = :erlang.system_time() / 1000
      time = finish - start
      result = {response.status_code, time, start, finish}
      Agent.update(agent, fn list -> [result|list] end)
    rescue
      e in HTTPotion.HTTPError ->
        # IO.puts "ERROR in Worker"
        # IO.inspect e
        Agent.update(agent, fn list -> [{-1,0,0,0}|list] end)
    end
    work(url, agent, requests_to_do - 1, opts)
  end
end
