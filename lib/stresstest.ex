defmodule Stresstest do
  require Worker
  require Reporter

  def main(args) do
    args |> parse_args |> process
  end

  defp parse_args(args) do
    {options, url, _} = OptionParser.parse(args,
      switches: [
        workers: :integer,
        requests: :integer
      ],
      aliases: [w: :workers, r: :requests]
    )
    {options, url}
  end

  defp process(args) do
    url = List.first(elem(args, 1))
    opts = elem(args, 0)
    num_of_workers = opts[:workers] || 1
    num_of_requests = opts[:requests] || 1

    headers = opts |>  Keyword.get_values(:header) |> parse_headers

    opts = opts
    |> Keyword.put(:total_requests, num_of_requests * num_of_workers)
    |> Keyword.put(:headers, headers)
    |> Keyword.put(:url, url)
    |> Keyword.put(:workers, num_of_workers)
    |> Keyword.put(:requests, num_of_requests)

    result = run_workers(url, num_of_workers, num_of_requests, opts)
    Reporter.render(result,opts)
  end

  defp parse_headers(headers_array) do
    headers_array
    |> Enum.map(fn(h) ->
      [key, value] = String.split(h, ":")
      {String.to_atom(key), value}
    end)
    |> Keyword.new
  end

  defp run_workers(url, num_of_workers, num_of_requests, opts) do
    IO.puts "Spawning #{num_of_workers} survey respondants @ #{num_of_requests} requests each..."
    {:ok, agent} = Agent.start_link(fn -> [] end)

    workers = for _ <- 1..num_of_workers, do:
      spawn fn -> Worker.call(agent, url, num_of_requests, opts)
    end
    wait_for_workers(workers, agent, num_of_requests * num_of_workers)
    IO.puts("") # clear after final progress bar
    Agent.get(agent, fn list -> list end)
  end

  defp wait_for_workers(workers, agent, total) do
    print_progress_bar(agent, total)
    aliveness = Enum.map(workers, fn(x) -> Process.alive?(x) end)
    if Enum.any?(aliveness, fn(x) -> x == true end) do
      :timer.sleep(250)
      wait_for_workers(workers, agent, total)
    end
  end

  defp print_progress_bar(agent, total) do
    results = Agent.get(agent, fn(list) -> list end)
    format = [
      bar_color: [IO.ANSI.white, IO.ANSI.green_background],
      blank_color: IO.ANSI.yellow_background,
    ]
    ProgressBar.render(length(results), total, format)
  end
end
