defmodule Reporter do
  def render(all_results, opts) do
    print_options(opts)

    successful_results = Enum.filter(all_results, fn(x) -> elem(x, 0) >= 200 && elem(x, 0) < 300 end)
    redirect_results = Enum.filter(all_results, fn(x) -> elem(x, 0) >= 300 && elem(x, 0) < 400 end)
    all_valid_results = Enum.filter(all_results, fn(x) -> elem(x, 0) >= 200 && elem(x, 0) < 400 end)
    failure_results = Enum.filter(all_results, fn(x) -> elem(x, 0) < 200 || elem(x, 0) >= 400 end)

    total_count = length(all_results)
    successful_count = length(successful_results)
    redirect_count = length(redirect_results)
    failure_count = length(failure_results)
    IO.puts "--==[ RESULTS ]==--"
    IO.puts " Success:   #{format_percentages(successful_count,total_count)}"
    IO.puts " Redirect:  #{format_percentages(redirect_count,total_count)}"
    IO.puts " Failure:   #{format_percentages(failure_count,total_count)}"
    print_times("Success",successful_results)
    print_times("Redirect",redirect_results)
    print_failure_summary failure_results
    
    print_benchmarks all_valid_results
  end

  defp print_options(opts) do
    IO.puts "--==[ CONFIGURATION ]==--"
    IO.puts(" Url: #{opts[:url]}")
    IO.puts(" Number of workers: #{opts[:workers]}")
    IO.puts(" Requests per worker: #{opts[:requests]}")
    IO.puts(" Total requests: #{opts[:total_requests]}")
  end

  defp print_benchmarks([]) do
  end

  defp print_benchmarks(result) do
    times = result |> Enum.map(&elem(&1, 1)) |> Enum.map(&(&1/1000))
    start = result |> Enum.map(&elem(&1, 2)) |> Enum.map(&(&1/1000)) |> Enum.min
    finish = result |> Enum.map(&elem(&1, 3)) |> Enum.map(&(&1/1000)) |> Enum.max
    total_duration = finish - start
    sum_of_times = Enum.sum(times)
    requests = length(result)
    IO.puts "--==[ BENCHMARKS ]==--"
    IO.puts " Test duration:               #{format_number(total_duration)} ms"
    IO.puts " Combined request duration:   #{format_number(sum_of_times)} ms"
    IO.puts " Requests per second:         #{format_number(requests/(total_duration/1000))} req/s"
  end

  defp print_times(_, []) do 
  end

  defp print_times(category, result) do
    times = result |> Enum.map(&elem(&1, 1)) |> Enum.map(&(&1/1000))
    max_val = Enum.max(times)
    min_val = Enum.min(times)
    avg_val = Enum.sum(times) / length(times)
    IO.puts "--==[ TIMES (#{category}) ]==--" 
    IO.puts " Max:      #{format_number(max_val)} ms"
    IO.puts " Min:      #{format_number(min_val)} ms"
    IO.puts " Avg:      #{format_number(avg_val)} ms"
  end

  defp print_failure_summary([]) do
  end

  defp print_failure_summary(result) do
    codes = Enum.reduce(result, %{}, fn x, acc -> Map.update(acc, elem(x,0), 1, &(&1 + 1)) end)
    IO.puts "--==[ FAILURE SUMMARY ]==--" 
    Enum.sort(codes, fn {_, left}, {_, right} -> left >= right end)
    |> Enum.each(fn {code, occurences} -> IO.puts " #{code}: #{occurences}" end)
  end

  defp format_percentages(_, 0) do
    "ERR_DIVIDE_BY_ZERO"
  end

  defp format_percentages(0, divisor) do
    "0/#{divisor} (0%)"
  end

  defp format_percentages(dividend, divisor) do
    "#{dividend}/#{divisor} (#{dividend/divisor * 100}%)"
  end

  defp format_number(num) do
    Float.round(num, 2)
  end
end
