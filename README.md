# RA stress test

Simple stress test.

## Requirements

Elixir >= 1.13

## Usage

Build:

```
mix deps.get
mix escript.build
```

Or if you're super lazy use `make build`. Both will output an executable named `ra_stress_test`.

Run:

```
./stresstest {URL} -w {NUMBER_OF_WORKERS} -r {NUMBER_OF_REQUESTS_PER_WORKER}
```

Example:

```
./stresstest https://ra.netigate-stage.com/s.aspx?s=633928X142387072X23637 -w 200 -r 5
```

* `-w` – number of workers to spawn (default: 10)
* `-r` – number of requests to open per worker (default: 1)

## License

Free for use by both genders.
